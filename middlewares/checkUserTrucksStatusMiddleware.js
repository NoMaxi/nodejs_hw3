const findTruckOnLoadByUser = require('../helpers/findTruckOnLoadByUser');
const {
  sendError,
  sendInternalServerError,
} = require('../helpers/responseHandler');

module.exports = (req, res, next) => {
  findTruckOnLoadByUser(req.user._id)
      .then((truck) => {
        if (!truck) {
          return next();
        }

        sendError(
            res,
            400,
            `Action can not be performed while user's truck is on load`
        );
      })
      .catch((err) => sendInternalServerError(res, err));
};
