const {sendError} = require('../helpers/responseHandler');

module.exports = (validator) => {
  return (req, res, next) => {
    const {error} = validator(req.body);

    if (error) {
      return sendError(res, 400, error.details[0].message);
    }

    next();
  };
};
