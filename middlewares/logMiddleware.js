const Log = require('../models/Log');
const {sendInternalServerError} = require('../helpers/responseHandler');

module.exports = (req, res, next) => {
  const {method, url} = req;
  const message = `Incoming request: method - '${method}', baseUrl - '${url}'.`;
  const log = new Log({message});

  log.save()
      .then(() => next())
      .catch((err) => sendInternalServerError(res, err));
};
