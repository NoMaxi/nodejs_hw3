const checkLoadNewStatusById = require('../helpers/checkLoadNewStatusById');
const {
  sendError,
  sendInternalServerError,
} = require('../helpers/responseHandler');

module.exports = (req, res, next) => {
  checkLoadNewStatusById(req.params.id)
      .then((load) => {
        if (!load) {
          return sendError(
              res,
              400,
              `Action can not be performed if load's status is not 'NEW'`
          );
        }

        next();
      })
      .catch((err) => sendInternalServerError(res, err));
};
