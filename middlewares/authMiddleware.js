const jwt = require('jsonwebtoken');

const {secret} = require('../config/auth');

module.exports = (req, res, next) => {
  const authHeader = req.headers['authorization'];

  if (!authHeader) {
    return res.status(401).json({message: 'No authorization header provided'});
  }

  const [, jwtToken] = authHeader.split(' ');

  try {
    req.user = jwt.verify(jwtToken, secret);
    next();
  } catch (err) {
    return res.status(401).json({message: 'Invalid JWT'});
  }
};
