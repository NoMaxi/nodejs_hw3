const {
  sendError,
  sendInternalServerError,
} = require('../helpers/responseHandler');

module.exports = (Model) => {
  return (req, res, next) => {
    const {id} = req.params;
    const userId = req.user._id.toString();

    Model.findById(id).exec()
        .then((model) => {
          if (!model) {
            return sendError(res, 404, `Item not found`);
          }

          if (model.created_by.toString() === userId ||
             (model.assigned_to && model.assigned_to.toString() === userId)) {
            return next();
          }

          const modelName = model.constructor.modelName;
          sendError(
              res,
              403,
              `Current ${req.user.role.toLowerCase()} does not have access ` +
              `to this ${modelName.toLowerCase()}`
          );
        })
        .catch((err) => sendInternalServerError(res, err));
  };
};
