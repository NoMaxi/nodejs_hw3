const {sendError} = require('../helpers/responseHandler');

module.exports = (...permittedRoles) => {
  return (req, res, next) => {
    const {user, user: {role}} = req;

    if (user && permittedRoles.includes(role)) {
      return next();
    }

    sendError(res, 403, `Access for ${role.toLowerCase()} is forbidden`);
  };
};
