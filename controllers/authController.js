const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const User = require('../models/User');
const generatePassword = require('../helpers/generatePassword');
const sendPasswordToEmail = require('../helpers/sendPasswordToEmail');
const {secret, saltRounds} = require('../config/auth');
const {
  sendError,
  sendInternalServerError,
  sendSuccess,
} = require('../helpers/responseHandler');

const register = (req, res) => {
  const {email, password, role} = req.body;

  bcrypt.hash(password, saltRounds)
      .then((hashedPassword) => {
        const user = new User({email, password: hashedPassword, role});
        return user.save();
      })
      .then(() => sendSuccess(res, {message: 'Profile created successfully'}))
      .catch((err) => sendInternalServerError(res, err));
};

const login = (req, res) => {
  const {email, password} = req.body;

  User.findOne({email}).select('email password role').exec()
      .then((user) => {
        if (!user) {
          return sendError(res, 404, 'No user with such email found');
        }

        return Promise.all([
          bcrypt.compare(password, user.password),
          Promise.resolve(user),
        ]);
      })
      .then(([arePasswordsEqual, user]) => {
        if (!arePasswordsEqual) {
          return sendError(res, 400, 'Incorrect password');
        }

        sendSuccess(res, {jwt_token: jwt.sign(JSON.stringify(user), secret)});
      })
      .catch((err) => sendInternalServerError(res, err));
};

const resetPassword = (req, res) => {
  const {email} = req.body;

  User.findOne({email}).exec()
      .then((user) => {
        if (!user) {
          return sendError(res, 404, 'No user with such email found');
        }

        const newPlainTextPassword = generatePassword();
        return Promise.all([
          bcrypt.hash(newPlainTextPassword, saltRounds),
          Promise.resolve(newPlainTextPassword),
          Promise.resolve(user),
        ]);
      })
      .then(([newHashedPassword, newPlainTextPassword, user]) => {
        user.password = newHashedPassword;

        return Promise.all([
          user.save(),
          sendPasswordToEmail(user.email, newPlainTextPassword),
        ]);
      })
      .then(() =>
        sendSuccess(res, {message: 'New password sent to your email address'}))
      .catch((err) => sendInternalServerError(res, err));
};

module.exports = {
  register,
  login,
  resetPassword,
};
