const Load = require('../models/Load');
const Truck = require('../models/Truck');
const {defaultLimit, defaultOffset} = require('../config/pagination');
const {states: loadStates} = require('../config/loads');
const loadLogger = require('../helpers/loadLogger');
const {
  sendError,
  sendInternalServerError,
  sendSuccess,
} = require('../helpers/responseHandler');

const getLoads = (req, res) => {
  const userId = req.user._id;
  const {
    status = {$exists: true},
    limit = defaultLimit,
    offset = defaultOffset,
  } = req.body;

  Load.find({$or: [{created_by: userId}, {assigned_to: userId}], status})
      .limit(limit)
      .skip(offset)
      .select('-__v')
      .exec()
      .then((loads) => sendSuccess(res, {loads}))
      .catch((err) => sendInternalServerError(res, err));
};

const addLoad = (req, res) => {
  const {_id: userId, role} = req.user;
  const {
    name,
    pickup_address,
    delivery_address,
    dimensions: {length, width, height},
    payload,
  } = req.body;
  const load = new Load({
    name,
    pickup_address,
    delivery_address,
    dimensions: {length, width, height},
    payload,
    created_by: userId,
  });

  loadLogger(
      load,
      `Load created successfully by ${role.toLowerCase()} with id ${userId}`
  );

  load.save()
      .then(() => {
        return sendSuccess(res, {message: 'Load created successfully'});
      })
      .catch((err) => sendInternalServerError(res, err));
};

const getLoadById = (req, res) => {
  const {id} = req.params;
  const userId = req.user._id;

  Load.findOne({_id: id, $or: [{created_by: userId}, {assigned_to: userId}]})
      .then((load) => sendSuccess(res, {load}))
      .catch((err) => sendInternalServerError(res, err));
};

const getActiveLoad = (req, res) => {
  const {_id: userId, role} = req.user;

  Load.findOne({assigned_to: userId, status: 'ASSIGNED'}).select('-__v').exec()
      .then((load) => {
        if (!load) {
          return sendError(
              res,
              400,
              `No active load found for ${role.toLowerCase()} ` +
              `with id ${userId}`);
        }

        sendSuccess(res, {load});
      })
      .catch((err) => sendInternalServerError(res, err));
};

const updateActiveLoadState = (req, res) => {
  const {_id: userId, role} = req.user;
  const finalState = loadStates[loadStates.length - 1];

  Load.findOne({assigned_to: userId, status: 'ASSIGNED'}).exec()
      .then((load) => {
        if (!load) {
          return sendError(
              res,
              400,
              `No active load found for ${role.toLowerCase()} ` +
              `with id '${userId}'`);
        }

        const currentStateIndex = loadStates.indexOf(load.state);
        load.state = loadStates[currentStateIndex + 1];
        loadLogger(load, `Load state changed to '${load.state}'`);

        return Promise.all([
          load.save(),
          Truck.findOne({assigned_to: userId}).exec(),
        ]);
      })
      .then(([load, truck]) => {
        if (load.state === finalState) {
          truck.status = 'IS';
          load.status = 'SHIPPED';
          load.state = finalState;
          loadLogger(
              load,
              `Load status changed to '${load.status}'`,
              `Load state changed to '${load.state}'.`
          );

          return Promise.all([load.save(), truck.save()]);
        }

        return Promise.all([Promise.resolve(load), Promise.resolve(truck)]);
      })
      .then(([load]) =>
        sendSuccess(
            res,
            {message: `Load state changed to '${load.state}'`}
        ))
      .catch((err) => sendInternalServerError(res, err));
};

const updateLoadById = (req, res) => {
  const {id} = req.params;

  Load.findByIdAndUpdate(id, req.body).exec()
      .then(() => sendSuccess(
          res, {message: 'Load details changed successfully'}
      ))
      .catch((err) => sendInternalServerError(res, err));
};

const deleteLoadById = (req, res) => {
  const {id} = req.params;

  Load.findByIdAndDelete(id).exec()
      .then(() => sendSuccess(res, {message: 'Load deleted successfully'}))
      .catch((err) => sendInternalServerError(res, err));
};

const postLoadById = (req, res) => {
  const {id} = req.params;
  const driverNotFoundMessage = `Driver not found for the load with id ${id}`;

  Load.findById(id).exec()
      .then((load) => {
        if (load.assigned_to) {
          return sendError(res, 400, 'Load is already assigned');
        }

        const {payload, dimensions: {length, width, height}} = load;
        const truckQuery = {
          'status': 'IS',
          'assigned_to': {$exists: true, $type: 7},
          'payload': {$gt: payload},
          'dimensions.length': {$gt: length},
          'dimensions.width': {$gt: width},
          'dimensions.height': {$gt: height},
        };

        load.status = 'POSTED';
        loadLogger(load, 'Load posted successfully');

        return Promise.all([
          Truck.findOne(truckQuery).sort('payload').exec(),
          load.save(),
        ]);
      })
      .then(([truck, load]) => {
        if (!truck) {
          load.status = 'NEW';
          loadLogger(
              load,
              driverNotFoundMessage,
              `Load status changed to ${load.status}`
          );

          return Promise.all([Promise.resolve(truck), load.save()]);
        }

        truck.status = 'OL';
        load.status = 'ASSIGNED';
        load.state = loadStates[0];
        load.assigned_to = truck.assigned_to;
        loadLogger(
            load,
            `Load assigned to driver with id ${truck.assigned_to}`,
            `Load status changed to ${load.state}`
        );

        return Promise.all([truck.save(), load.save()]);
      })
      .then(([truck]) => {
        if (!truck) {
          return sendError(res, 400, driverNotFoundMessage);
        }

        return sendSuccess(res, {
          message: 'Load posted successfully',
          driver_found: true,
        });
      })
      .catch((err) => sendInternalServerError(res, err));
};

const getLoadShippingInfoById = (req, res) => {
  const {id} = req.params;

  Load.findOne({_id: id, status: {$in: ['ASSIGNED', 'SHIPPED']}})
      .select('-__v')
      .exec()
      .then((load) => {
        if (!load) {
          return sendError(
              res, 404, `No shipping info found for load with id ${id}`
          );
        }

        return Promise.all([
          Promise.resolve(load),
          Truck.findOne({assigned_to: load.assigned_to})
              .select('-__v -dimensions -payload').exec(),
        ]);
      })
      .then(([load, truck]) => sendSuccess(res, {load, truck}))
      .catch((err) => sendInternalServerError(res, err));
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  updateActiveLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingInfoById,
};
