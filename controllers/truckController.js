const Truck = require('../models/Truck');
const {types: truckTypes} = require('../config/trucks');
const {
  sendError,
  sendInternalServerError,
  sendSuccess,
} = require('../helpers/responseHandler');

const getTrucks = (req, res) => {
  Truck.find({created_by: req.user._id})
      .select('-__v -dimensions -payload').exec()
      .then((trucks) => {
        sendSuccess(res, {trucks});
      })
      .catch((err) => sendInternalServerError(res, err));
};

const addTruck = (req, res) => {
  const truck = new Truck({type: req.body.type, created_by: req.user._id});

  truck.save()
      .then(() => sendSuccess(res, {message: 'Truck created successfully'}))
      .catch((err) => sendInternalServerError(res, err));
};

const getTruckById = (req, res) => {
  const {id} = req.params;

  Truck.findById(id).select('-__v -dimensions -payload').exec()
      .then((truck) => sendSuccess(res, {truck}))
      .catch((err) => sendInternalServerError(res, err));
};

const updateTruckById = (req, res) => {
  const {id} = req.params;
  const {type} = req.body;

  Truck.findById(id).exec()
      .then((truck) => {
        if (type === truck.type) {
          return sendError(res, 400, 'Truck type is the same as previous one');
        }

        const newTruckType = truckTypes.find((el) => el.name === type);
        truck.type = type;
        truck.dimensions = newTruckType.dimensions;
        truck.payload = newTruckType.payload;

        return truck.save();
      })
      .then(() => {
        return sendSuccess(
            res,
            {message: 'Truck details changed successfully'}
        );
      })
      .catch((err) => {
        sendInternalServerError(res, err);
      });
};

const deleteTruckById = (req, res) => {
  const {id} = req.params;

  Truck.findByIdAndDelete(id).exec()
      .then(() => sendSuccess(res, {message: 'Truck deleted successfully'}))
      .catch((err) => sendInternalServerError(res, err));
};

const assignTruckById = (req, res) => {
  const {id} = req.params;
  const userId = req.user._id;

  Truck.findById(id).exec()
      .then((truck) => {
        return Promise.all([
          Truck.findOne({assigned_to: userId}).exec(),
          Promise.resolve(truck),
        ]);
      })
      .then(([previouslyAssignedTruck, truck]) => {
        if (previouslyAssignedTruck) {
          if (id === previouslyAssignedTruck._id.toString()) {
            return sendError(
                res,
                400,
                'Truck is already assigned to this driver'
            );
          }

          previouslyAssignedTruck.assigned_to = undefined;
          previouslyAssignedTruck.status = 'OS';
          truck.assigned_to = userId;
          truck.status = 'IS';

          return Promise.all([
            previouslyAssignedTruck.save(),
            truck.save(),
          ]);
        }

        truck.assigned_to = userId;
        truck.status = 'IS';
        return truck.save();
      })
      .then(() => sendSuccess(res, {message: 'Truck assigned successfully'}))
      .catch((err) => sendInternalServerError(res, err));
};

module.exports = {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
