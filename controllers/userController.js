const bcrypt = require('bcryptjs');
const formidable = require('formidable');
const fsPromises = require('fs').promises;

const User = require('../models/User');
const {saltRounds} = require('../config/auth');
const {
  sendError,
  sendInternalServerError,
  sendSuccess,
} = require('../helpers/responseHandler');

const getUser = (req, res) => {
  const {_id: id} = req.user;

  User.findById(id).select('_id email created_date').exec()
      .then((user) => {
        if (!user) {
          return sendError(res, 404, 'User not found');
        }

        sendSuccess(res, {user});
      })
      .catch((err) => sendInternalServerError(res, err));
};

const changeUserPassword = (req, res) => {
  const {oldPassword, newPassword} = req.body;

  bcrypt.hash(newPassword, saltRounds)
      .then((hashedNewPassword) => {
        return User
            .findByIdAndUpdate(req.user._id, {password: hashedNewPassword})
            .exec();
      })
      .then((user) => {
        if (!user) {
          return sendError(res, 404, 'User not found');
        }

        return bcrypt.compare(oldPassword, user.password);
      })
      .then((arePasswordsEqual) => {
        if (!arePasswordsEqual) {
          return sendError(res, 400, 'Incorrect old password');
        }

        sendSuccess(res, {message: 'Password changed successfully'});
      })
      .catch((err) => sendInternalServerError(res, err));
};

const deleteUser = (req, res) => {
  User.findByIdAndDelete(req.user._id).exec()
      .then((user) => {
        if (!user) {
          return sendError(res, 404, 'User not found');
        }

        sendSuccess(res, {message: 'Profile deleted successfully'});
      })
      .catch((err) => sendInternalServerError(res, err));
};

const getUserAvatar = (req, res) => {
  User.findById(req.user._id).exec()
      .then((user) => {
        if (!user) {
          return sendError(res, 404, 'User not found');
        }

        if (!user.avatar || !user.avatar.data) {
          return sendError(
              res, 400, 'No avatar image uploaded for current user'
          );
        }

        res.contentType(user.avatar.contentType);
        res.status(200).send(user.avatar.data);
      })
      .catch((err) => {
        return sendInternalServerError(res, err);
      });
};

const uploadUserAvatar = (req, res) => {
  const form = formidable();

  form.parse(req, (err, fields, files) => {
    if (err) {
      return sendError(res, 400, 'Avatar image upload failed');
    }

    if (files[''].size === 0) {
      return sendError(res, 400, 'Avatar image not provided');
    }

    if (!files[''].type.startsWith('image')) {
      return sendError(res, 400, 'Incorrect avatar image content-type');
    }

    User.findById(req.user._id).exec()
        .then((user) => {
          if (!user) {
            return sendError(res, 404, 'User not found');
          }

          return Promise.all([
            Promise.resolve(user),
            fsPromises.readFile(files[''].path),
          ]);
        })
        .then(([user, avatarImage]) => {
          user.avatar.contentType = files[''].type;
          user.avatar.data = avatarImage;

          return user.save();
        })
        .then(() => {
          sendSuccess(res, {message: 'Avatar image uploaded successfully'});
        })
        .catch((err) => sendInternalServerError(res, err));
  });
};

const deleteUserAvatar = (req, res) => {
  User.findById(req.user._id).exec()
      .then((user) => {
        if (!user) {
          return sendError(res, 404, 'User not found');
        }

        if (!user.avatar || !user.avatar.data) {
          return sendError(
              res, 400, 'No avatar image uploaded for current user'
          );
        }

        user.avatar = undefined;

        return user.save();
      })
      .then(() => {
        return sendSuccess(res, {message: 'Avatar image deleted successfully'});
      })
      .catch((err) => sendInternalServerError(res, err));
};

module.exports = {
  getUser,
  changeUserPassword,
  deleteUser,
  getUserAvatar,
  uploadUserAvatar,
  deleteUserAvatar,
};
