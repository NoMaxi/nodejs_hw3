require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const logMiddleware = require('./middlewares/logMiddleware');
const authMiddleware = require('./middlewares/authMiddleware');
const {basePath, port} = require('./config/server');
const dbConfig = require('./config/database');

const {dbName, dbHost, dbCluster, dbUsername, dbPassword} = dbConfig;

mongoose.connect(
    `${dbHost}://${dbUsername}:${dbPassword}@${dbCluster}/${dbName}?retryWrites=true&w=majority`,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    })
    .then(() => {
      console.log(`Connected to database ${dbHost}`);
    });

const app = express();

app.use(express.static('./public'));
app.use(cors());
app.use(express.json());

app.use(logMiddleware);
const allRoutesExceptAuthRegexp = new RegExp(`^((?!${basePath}/auth).)*$`);
app.use(allRoutesExceptAuthRegexp, authMiddleware);

const authRouter = require('./routes/authRouter');
const userRouter = require('./routes/userRouter');
const truckRouter = require('./routes/truckRouter');
const loadRouter = require('./routes/loadRouter');

app.use(basePath, authRouter);
app.use(basePath, userRouter);
app.use(basePath, truckRouter);
app.use(basePath, loadRouter);

app.listen(port, () => {
  console.log(`Server started on ${port} port`);
});
