module.exports = {
  statuses: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  states: [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery',
  ],
};
