const config = require('config');
const {name, host, cluster, username, password} = config.get('db');

module.exports = {
  dbName: process.env.DB_NAME || name,
  dbHost: process.env.DB_HOST || host,
  dbCluster: process.env.DB_CLUSTER || cluster,
  dbUsername: process.env.DB_USERNAME || username,
  dbPassword: process.env.DB_PASSWORD || password,
};
