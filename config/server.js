const config = require('config');
const {host, port, basePath} = config.get('server');

module.exports = {
  host: process.env.HOST || host,
  port: process.env.PORT || port,
  basePath: process.env.BASE_PATH || basePath,
};
