const config = require('config');
const {domain, apiKey} = config.get('mailAuth');

module.exports = {
  domain: process.env.MAIL_DOMAIN || domain,
  apiKey: process.env.MAIL_API_KEY || apiKey,
};
