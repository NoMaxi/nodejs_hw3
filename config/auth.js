const config = require('config');
const {secret, saltRounds} = config.get('auth');

module.exports = {
  secret: process.env.AUTH_SECRET || secret,
  saltRounds: +process.env.AUTH_SALT_ROUNDS || +saltRounds,
};
