module.exports = {
  types: [
    {
      name: 'SPRINTER',
      dimensions: {
        length: 300,
        width: 250,
        height: 170,
      },
      payload: 1700,
    },
    {
      name: 'SMALL STRAIGHT',
      dimensions: {
        length: 500,
        width: 250,
        height: 170,
      },
      payload: 2500,
    },
    {
      name: 'LARGE STRAIGHT',
      dimensions: {
        length: 700,
        width: 350,
        height: 200,
      },
      payload: 4000,
    },
  ],
  statuses: ['OS', 'IS', 'OL'],
};
