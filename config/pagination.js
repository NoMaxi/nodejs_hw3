const config = require('config');
const {defaultLimit, maxLimit, offset} = config.get('pagination');

module.exports = {
  defaultLimit: +process.env.PAGINATION_DEFAULT_LIMIT || +defaultLimit,
  maxLimit: +process.env.PAGINATION_MAX_LIMIT || +maxLimit,
  defaultOffset: +process.env.PAGINATION_OFFSET || +offset,
};
