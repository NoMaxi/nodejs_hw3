const express = require('express');
const router = express.Router();

const Load = require('../models/Load');
const permitRoleMiddleware = require('../middlewares/permitRoleMiddleware');
const permitCurrentUserMiddleware =
    require('../middlewares/permitCurrentUserMiddleware');
const checkLoadStatusMiddleware =
    require('../middlewares/checkLoadStatusMiddleware');
const validateMiddleware = require('../middlewares/validateMiddleware');
const {
  validateLoad,
  validateGetLoads,
  validateUpdateLoad,
} = require('../helpers/validators/loadValidator');
const {
  getLoads,
  addLoad,
  getActiveLoad,
  updateActiveLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingInfoById,
} = require('../controllers/loadController');

router.get('/loads', validateMiddleware(validateGetLoads), getLoads);
router.post(
    '/loads',
    permitRoleMiddleware('SHIPPER'),
    validateMiddleware(validateLoad),
    addLoad
);
router.get('/loads/active', permitRoleMiddleware('DRIVER'), getActiveLoad);
router.patch(
    '/loads/active/state',
    permitRoleMiddleware('DRIVER'),
    updateActiveLoadState
);

router.use('/loads/:id', permitCurrentUserMiddleware(Load));

router.get('/loads/:id', getLoadById);

router.use('/loads/:id', permitRoleMiddleware('SHIPPER'));

router.get('/loads/:id/shipping_info', getLoadShippingInfoById);

router.use('/loads/:id', checkLoadStatusMiddleware);

router.put(
    '/loads/:id',
    validateMiddleware(validateUpdateLoad),
    updateLoadById
);
router.delete('/loads/:id', deleteLoadById);
router.post('/loads/:id/post', postLoadById);

module.exports = router;
