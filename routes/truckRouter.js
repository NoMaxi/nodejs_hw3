const express = require('express');
const router = express.Router();

const Truck = require('../models/Truck');
const permitRoleMiddleware = require('../middlewares/permitRoleMiddleware');
const permitCurrentUserMiddleware =
    require('../middlewares/permitCurrentUserMiddleware');
const checkUserTrucksStatusMiddleware =
    require('../middlewares/checkUserTrucksStatusMiddleware');
const validateMiddleware = require('../middlewares/validateMiddleware');
const {
  validateTruck,
  validateTruckUpdate,
} = require('../helpers/validators/truckValidator');

const {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
} = require('../controllers/truckController');

router.use('/trucks', permitRoleMiddleware('DRIVER'));

router.get('/trucks', getTrucks);
router.post('/trucks', validateMiddleware(validateTruck), addTruck);

router.use('/trucks/:id', permitCurrentUserMiddleware(Truck));
router.use('/trucks/:id', checkUserTrucksStatusMiddleware);

router.get('/trucks/:id', getTruckById);
router.put(
    '/trucks/:id',
    validateMiddleware(validateTruckUpdate),
    updateTruckById
);
router.delete('/trucks/:id', deleteTruckById);
router.post('/trucks/:id/assign', assignTruckById);

module.exports = router;
