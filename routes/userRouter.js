const express = require('express');
const router = express.Router();

const checkUserTrucksStatusMiddleware =
    require('../middlewares/checkUserTrucksStatusMiddleware');
const validateMiddleware = require('../middlewares/validateMiddleware');
const {
  validateChangePassword,
} = require('../helpers/validators/userValidator');

const {
  getUser,
  changeUserPassword,
  deleteUser,
  getUserAvatar,
  uploadUserAvatar,
  deleteUserAvatar,
} = require('../controllers/userController');

router.get('/users/me', getUser);

router.use('/users/me', checkUserTrucksStatusMiddleware);

router.delete('/users/me', deleteUser);
router.patch(
    '/users/me/password',
    validateMiddleware(validateChangePassword),
    changeUserPassword
);
router.get('/users/me/avatar', getUserAvatar);
router.post('/users/me/avatar', uploadUserAvatar);
router.delete('/users/me/avatar', deleteUserAvatar);

module.exports = router;
