const express = require('express');
const router = express.Router();

const validateMiddleware = require('../middlewares/validateMiddleware');
const {
  validateUser,
  validateLogin,
  validateResetPassword,
} = require('../helpers/validators/userValidator');
const {
  register,
  login,
  resetPassword,
} = require('../controllers/authController');

router.post('/auth/register', validateMiddleware(validateUser), register);
router.post('/auth/login', validateMiddleware(validateLogin), login);
router.post(
    '/auth/forgot_password',
    validateMiddleware(validateResetPassword),
    resetPassword
);

module.exports = router;
