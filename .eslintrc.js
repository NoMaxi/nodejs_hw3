module.exports = {
  'env': {
    'browser': true,
    'es6': true,
    'node': true,
  },
  'parserOptions': {
    'ecmaFeatures': {
      'experimentalObjectRestSpread': true,
      'jsx': true,
    },
    'sourceType': 'module',
  },
  'extends': ['eslint:recommended', 'google'],
  'rules': {
    'linebreak-style': [
      'error',
      (process.platform === 'win32' ? 'windows' : 'unix'),
    ],
    'require-jsdoc': [2, {
      'require': {
        'FunctionDeclaration': false,
        'MethodDefinition': false,
        'ClassDeclaration': false,
      },
    }],
    'new-cap': ['error', {'capIsNewExceptions': ['Router']}],
    'camelcase': ['error', {
      'properties': 'never',
      'ignoreDestructuring': true,
    }],
  },
};
