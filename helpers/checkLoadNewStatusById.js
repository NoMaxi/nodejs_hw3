const Load = require('../models/Load');

module.exports = (loadId) => {
  return Load.findOne({_id: loadId, status: 'NEW'}).exec();
};
