const {Types: {ObjectId}} = require('mongoose');

module.exports = (objectId) => {
  const objectIdRegexp = new RegExp(/^[a-f\d]{24}$/i);
  if (typeof objectId === 'string') {
    return objectIdRegexp.test(objectId);
  }

  return objectId instanceof ObjectId &&
         objectIdRegexp.test(objectId.toString());
};
