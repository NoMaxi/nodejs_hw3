const Truck = require('../models/Truck');

module.exports = (driverId) => {
  return Truck.findOne({created_by: driverId, status: 'OL'}).exec();
};
