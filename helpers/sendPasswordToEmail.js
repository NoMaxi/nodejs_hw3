const mailgun = require('mailgun-js');

const {domain, apiKey} = require('../config/mailAuth');

module.exports = (email, password) => {
  const mg = mailgun({apiKey, domain});
  const data = {
    from: 'no-reply@samples.mailgun.org',
    to: email,
    subject: 'Reset user password',
    text: `Hello, your password has been reset. Your new password: ${password}`,
    html: '<p>Hello, your password has been reset.' +
        `Your new password: <b>${password}</b></p>`,
  };

  return mg.messages().send(data);
};
