const joi = require('joi');

const {roles} = require('../../config/users');

const validateUser = (body) => {
  const schema = joi.object({
    email: joi.string().min(5).max(35).email({minDomainSegments: 2}).required(),
    password: joi.string().min(3).max(64).required(),
    role: joi.string().valid(...roles).required(),
    created_date: joi.date(),
  });

  return schema.validate(body);
};

const validateLogin = (body) => {
  const schema = joi.object({
    email: joi.string().min(5).max(35).email({minDomainSegments: 2}).required(),
    password: joi.string().min(3).max(64).required(),
  });

  return schema.validate(body);
};

const validateChangePassword = (body) => {
  const schema = joi.object({
    oldPassword: joi.string().min(3).max(64).required(),
    newPassword: joi.string().min(3).max(64).required(),
  });

  return schema.validate(body);
};

const validateResetPassword = (body) => {
  const schema = joi.object({
    email: joi.string().min(5).max(35).email({minDomainSegments: 2}).required(),
  });

  return schema.validate(body);
};

module.exports = {
  validateUser,
  validateLogin,
  validateResetPassword,
  validateChangePassword,
};
