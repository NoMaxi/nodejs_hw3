const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);

const {types} = require('../../config/trucks');

const truckNames = types.map((truck) => truck.name);

const validateTruck = (body) => {
  const schema = joi.object({
    type: joi.string().valid(...truckNames).required(),
  });

  return schema.validate(body);
};

const validateTruckUpdate = (body) => {
  const schema = joi.object({
    type: joi.string().valid(...truckNames).required(),
  });

  return schema.validate(body);
};

module.exports = {
  validateTruck,
  validateTruckUpdate,
};
