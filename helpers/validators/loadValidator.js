const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);

const {statuses} = require('../../config/loads');
const {maxLimit} = require('../../config/pagination');

const validateLoad = (body) => {
  const schema = joi.object({
    name: joi.string().required(),
    pickup_address: joi.string().required(),
    delivery_address: joi.string().required(),
    payload: joi.number().required(),
    dimensions: joi.object({
      length: joi.number().required(),
      width: joi.number().required(),
      height: joi.number().required(),
    }),
  });

  return schema.validate(body);
};

const validateGetLoads = (body) => {
  const schema = joi.object({
    status: joi.string().valid(...statuses),
    limit: joi.number().max(maxLimit),
    offset: joi.number(),
  });

  return schema.validate(body);
};

const validateUpdateLoad = (body) => {
  const schema = joi.object({
    name: joi.string(),
    pickup_address: joi.string(),
    delivery_address: joi.string(),
    payload: joi.number(),
    dimensions: joi.object({
      length: joi.number(),
      width: joi.number(),
      height: joi.number(),
    }),
  });

  return schema.validate(body);
};

module.exports = {
  validateLoad,
  validateGetLoads,
  validateUpdateLoad,
};
