const cryptoRandomString = require('crypto-random-string');
const PASSWORD_DEFAULT_LENGTH = 10;

module.exports = (passwordLength = PASSWORD_DEFAULT_LENGTH) => {
  return cryptoRandomString({length: passwordLength, type: 'base64'});
};
