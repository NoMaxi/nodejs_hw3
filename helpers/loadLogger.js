module.exports = (load, ...messages) => {
  for (const message of messages) {
    load.logs.push({message, time: Date.now()});
  }
};
