const sendError = (res, status, message) => {
  return res.status(status).json({message});
};

const sendInternalServerError = (res, err) => {
  return sendError(res, 500, err.message);
};

const sendSuccess = (res, body = {message: 'Success'}) => {
  return res.json(body);
};

module.exports = {
  sendError,
  sendInternalServerError,
  sendSuccess,
};
