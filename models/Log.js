const {Schema, model} = require('mongoose');

const logSchema = new Schema({
  message: {
    type: String,
    required: true,
  },
  time: {
    type: Date,
    default: Date.now,
  },
});

module.exports = model('Log', logSchema);
