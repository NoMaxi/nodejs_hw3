const {Schema, Types, model} = require('mongoose');
const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);

const {types, statuses} = require('../config/trucks');

const truckNames = types.map((truck) => truck.name);
const truckLengths = types.map((truck) => truck.dimensions.length);
const truckWidths = types.map((truck) => truck.dimensions.width);
const truckHeights= types.map((truck) => truck.dimensions.height);
const truckPayloads = types.map((truck) => truck.payload);

const truckSchema = new Schema({
  type: {
    type: String,
    required: true,
    enum: truckNames,
  },
  created_by: {
    type: Types.ObjectId,
    required: true,
    ref: 'User',
  },
  assigned_to: {
    type: Types.ObjectId,
    ref: 'User',
  },
  status: {
    type: String,
    enum: statuses,
    default: statuses[0],
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
  dimensions: {
    length: {
      type: Number,
      enum: truckLengths,
      default: function() {
        return types.find((el) => el.name === this.type).dimensions.length;
      },
    },
    width: {
      type: Number,
      enum: truckWidths,
      default: function() {
        return types.find((el) => el.name === this.type).dimensions.width;
      },
    },
    height: {
      type: Number,
      enum: truckHeights,
      default: function() {
        return types.find((el) => el.name === this.type).dimensions.height;
      },
    },
  },
  payload: {
    type: Number,
    enum: truckPayloads,
    default: function() {
      return types.find((el) => el.name === this.type).payload;
    },
  },
});

module.exports = model('Truck', truckSchema);
