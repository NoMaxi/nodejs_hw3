const {Schema, Types, model} = require('mongoose');
const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);

const {statuses, states} = require('../config/loads');

const loadSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
  created_by: {
    type: Types.ObjectId,
    ref: 'User',
  },
  logs: [{
    _id: false,
    message: {
      type: String,
    },
    time: {
      type: Date,
      default: Date.now,
    },
  }],
  assigned_to: {
    type: Types.ObjectId,
    ref: 'User',
  },
  status: {
    type: String,
    enum: statuses,
    default: statuses[0],
  },
  state: {
    type: String,
    enum: states,
  },
  dimensions: {
    length: {
      type: Number,
      required: true,
    },
    width: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
    trim: true,
  },
  delivery_address: {
    type: String,
    required: true,
    trim: true,
  },
});

module.exports = model('Load', loadSchema);
