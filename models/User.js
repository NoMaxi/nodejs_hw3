const {Schema, model} = require('mongoose');

const {roles} = require('../config/users');

const userSchema = new Schema({
  email: {
    type: String,
    required: [true, 'Please provide email'],
    unique: true,
    trim: true,
    lowercase: true,
    minLength: 5,
    maxlength: 35,
  },
  password: {
    type: String,
    required: [true, 'Please provide password'],
    minLength: 3,
    maxlength: 64,
  },
  role: {
    type: String,
    required: true,
    enum: roles,
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
  avatar: {
    data: Buffer,
    contentType: String,
  },
});

module.exports = model('User', userSchema);
