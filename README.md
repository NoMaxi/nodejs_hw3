## NODEJS_HW3 - Shipment delivery application 

#### Requirements
**[NodeJS](https://nodejs.org/en/)** must be installed on your machine.

#### Application launch
Run commands in the terminal:
1. **`npm install`**
2. **`npm run start`**

#### API documentation
Detailed API documentation can be found in `swagger.yaml` in root directory - paste its
 content into the editor window on **[Swagger](https://editor.swagger.io/)**.
 
#### Environment variables example
Environment variables example can be found in `env.example` in root directory.
